
import numpy as np
import pandas as pd
import datetime

from sklearn.linear_model import SGDClassifier as SGDClassifier
from sklearn.model_selection import train_test_split

from NetworkOps.Meinhardt import Meinhardt_network
from NetworkOps.InputLayer import spatially_correlated_input_layer, random_input_layer
from SpikingSimulators.AsynchronousSimulation import asynchronous_simulation


class SNN:

    def __init__(
            self,
            N, use_phase,
            out_halflife,
            drive_tau,
            capacity,
            ref_voltage,
            ss_voltage,
            inhibition,
            synapse_str,
            i_to_e_distances,
            i_to_e_densities,
            density_e,
            periodic,
            mean_e_e,
            n_inputs,
            sampling_rate,
            in_connectivity,
            lam_out,
            n_out,
            readout_tau,
            v_store_for):

        # Calculations for the creation of the reccurent layer
        r_e = np.sqrt(mean_e_e / (N * np.pi * density_e))
        if inhibition != 0:
            r_i = i_to_e_distances * r_e
            rr = r_i / r_e
            # Calculate the sparsity of inhibitory connections that would yield a balanced network
            balanced_density_i = density_e * (1 - inhibition) / (rr ** 2 * inhibition)
            # Adjust the density according to user specs
            density_i = balanced_density_i * i_to_e_densities
        else:
            density_i, r_i = 0, 0

        # Build the input network
        input_layer_specs = {'n_units': N,
                             'n_inputs': n_inputs,
                             'connectivity': in_connectivity}
        input_layer = random_input_layer(**input_layer_specs)

        # Build the recurrent network
        net_specs = {'n': N, 'inhibition': inhibition,
                     'l_e': r_e, 'l_i': r_i,
                     'density_e': density_e, 'density_i': density_i,
                     'synapse_sigma': 0, 'synapse_strength': synapse_str,
                     'periodic': periodic, 'lloyd_iter': 0}
        A, x_pos, y_pos, neuron_type = Meinhardt_network(**net_specs)

        # Build the readout network
        output_layer_specs = {'x': x_pos, 'y': y_pos,
                              'periodic_boundaries': periodic,
                              'n_inputs': n_out,
                              'lam': lam_out}
        output_layer = spatially_correlated_input_layer(**output_layer_specs)

        # Store attributes
        self.n_inputs = n_inputs
        self.n_out = n_out
        self.n_recc = N
        self.use_phase = use_phase
        self.capacity = capacity
        self.ref_voltage = ref_voltage
        self.readout_tau = readout_tau
        self.drive_tau = drive_tau
        self.ss_voltage = ss_voltage
        self.v_store_for = v_store_for
        self.sampling_rate = sampling_rate
        self.recurrent_layer = A
        self.output_layer = output_layer
        self.input_layer = input_layer
        self.out_halflife = out_halflife
        self.classifier = None


    def _run_snn(self, sample):
        output = asynchronous_simulation(
            sample=sample,
            use_phase=self.use_phase,
            capacity=self.capacity,
            ref_voltage=self.ref_voltage,
            readout_tau=self.readout_tau,
            drive_tau=self.drive_tau,
            ss_voltage=self.ss_voltage,
            v_store_for=self.v_store_for,
            sampling_rate=self.sampling_rate,
            recurrent_layer=self.recurrent_layer,
            output_layer=self.output_layer,
            input_layer=self.input_layer)

        sampled_reucc_state, output_state, cascade_size = output

        return sampled_reucc_state, output_state, cascade_size


    def _transform_samples(self, samples, verbose=False):

        n_samples = samples.shape[0]

        transformed = np.zeros((n_samples, self.n_inputs))
        for i, sample in enumerate(samples):

            temp = datetime.datetime.now()

            _, snn_output, _ = self._run_snn(sample)
            snn_output = pd.DataFrame(snn_output).ewm(
                halflife=self.out_halflife, axis=0).mean().values
            transformed[i, :] = snn_output[-1, :]

            total_time = datetime.datetime.now() - temp

            if verbose: print('Sample ' + str(i) + ' took ' + str(total_time) + ' to transform.')

        return transformed


    def train(self, samples, test_fraction, labels, verbose=False):
        # First, pass the inputs through the SNN
        # then, use the SNN output to train a perceptrion
        # We will use the final state of the SNN's output
        # Finally, we test the out-of-sample accuracy on a fraction of the samples

        # Step 1: use the SNN
        if verbose: print('SNN transformation of inputs ##########')
        transformed = self._transform_samples(samples, verbose=verbose)
        X_train, X_test, y_train, y_test = train_test_split(
            transformed, labels,
            test_size=test_fraction,
            random_state=0)

        # Step 2: train the readout
        if verbose: print('Training classifier layer ##########')
        temp = datetime.datetime.now()

        self.readout = SGDClassifier(tol=1e-3, random_state=0).fit(transformed, labels)

        total_time = datetime.datetime.now() - temp
        if verbose: print('Classifier took ' + str(total_time) + ' to train.')
        if verbose: print(self.readout.score(transformed, labels))

        # Step 3:
        self.in_sample = self.readout.score(X_train, y_train)
        self.out_sample = self.readout.score(X_test, y_test)
        if verbose: print('In/Out sample accuracies were: ' + str(self.in_sample) + ',  ' + str(self.out_sample))


    def predict(self, samples):
        assert self.classifier is not None, 'Need to train the network first.'

        # Step 1: transform the input (samples) using the spiking neural network.
        transformed = self._transform_samples(samples, verbose=verbose)

        # Step 2: Pass the output of the spiking neural network to (trained) the readout layer.
        predictions = self.readout.predict(transformed)

        return predictions

