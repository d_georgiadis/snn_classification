import numpy as np


def Herfindahl(xx):
    x = xx*1./np.sum(xx)
    H = np.sum(x**2)
    N = len(x)
    return  (H-1/N) / (1-1/N)  #np.sum((x/np.sum(x))**2)/len(x)


