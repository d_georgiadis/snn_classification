import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
import os
import click

from StorageOps.io_stream import load
from VisualisationOps.VoronoiPlots import voronoi_plot

# ffmpeg -i snapshot_%d.png -c:v libx264 -vf fps=25 -pix_fmt yuv420p out.mp4

def quickvideo(filepath):
    data = load(filepath)

    # Create a file for data storage
    fname = filepath.split('\\')[-1].split('/')[-1]
    store_in = 'video_' + fname
    try:
        os.makedirs(store_in)
    except:
        if click.confirm('File exists. Overwrite?', default=True):
            print('Overwritting...')
        else:
            print('Stopping.')
            return

    # Make all the plots
    phi_snapshot = np.zeros(len(data['x_pos']))
    extremum = 1.1

    plt.figure(figsize=(10, 10))
    for i, row in enumerate(data['voltages_levels']):

        plt.clf(); plt.yticks([]); plt.xticks([]); plt.axis('off')

        #plt.scatter(x=data['x_pos'], y=data['y_pos'], s=8,
        #            c=np.clip(row, -extremum, extremum),
        #            vmin=-extremum, vmax=extremum, cmap='seismic')
        plt.figure(figsize=(10, 10))
        voronoi_plot(x=data['x_pos'],
                     y=data['y_pos'],
                     z=np.clip(row, -extremum, extremum),
                     ax=plt.gca(),
                     cmap=cm.seismic,
                     extremum=1,
                     divergent=True)

        plt.tight_layout()
        plt.savefig(store_in+'\\snapshot_'+str(i)+'.png')
        plt.close()

quickvideo(r'\Users\DGeorgiadis\PycharmProjects\Brian2\Experiments\Pilot5_5\e_e=14_i_to_e_densities=1_r=3.hdf5')

