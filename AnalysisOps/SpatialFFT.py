import numpy as np
import matplotlib.pyplot as plt


def imagefy(x, y, z, resolution, nans=True):
    """
    Given some Z values at arbitrary positions X,Y: calcualtes the mean inside the cells of 2D square lattice.
    """
    tot, _, _ = np.histogram2d(x, y, bins=resolution, weights=z)
    num, _, _ = np.histogram2d(x, y, bins=resolution)
    img = tot / num

    if not nans: img[np.isnan(img)] = 0

    return img


def apply_spatial_fft(image):
    # Take the 2-dimensional DFT and centre the frequencies
    ftimage = np.fft.fft2(image)
    ftimage = np.fft.fftshift(ftimage)
    return ftimage


def surfplot(Z, X=None, Y=None, ax=None):
    from mpl_toolkits.mplot3d import Axes3D
    from matplotlib import cm
    from matplotlib.ticker import LinearLocator, FixedLocator, FormatStrFormatter

    if X is None or Y is None:
        X, Y = np.mgrid[:len(Z), :len(Z)][0], np.mgrid[:len(Z), :len(Z)][1]

    if ax is None:
        fig = plt.figure(figsize=(7, 5))
        ax = fig.gca(projection='3d')

    surf = ax.plot_surface(X, Y, Z, rstride=1, cstride=1, cmap=cm.jet,
                           linewidth=0, antialiased=False)

    ax.w_zaxis.set_major_locator(LinearLocator(10))
    ax.w_zaxis.set_major_formatter(FormatStrFormatter('%.03f'))

    # fig.colorbar(surf, shrink=0.5, aspect=5)
    plt.suptitle('2D power spectrum (log)')
    plt.show()


def spatial_power_spectrum(x, y, zeta, capacity, nbins, howmany=100, plot=False, demean=False):
    import gc

    # Demean if requested
    if demean: zeta = zeta - np.mean(zeta)

    # Get the 2d FFT for each timestep of zeta and them mean them
    ps_2d_list = []
    stored_instances = np.unique(zeta.nonzero()[0])
    c = 0
    for t in stored_instances[-howmany:]:

        c+=1
        if c%10==0: gc.collect()

        z_t = zeta.getrow(t).copy()
        z_t = z_t.toarray()
        z_t = z_t[0]
        z_t[z_t > capacity] = 0

        img = imagefy(x, y, z_t, nbins, nans=False)
        ps_2d_list += [np.abs(apply_spatial_fft(img) ** 2)]

    # Average the spectra
    spatial_ps = np.mean(ps_2d_list, axis=0)

    if plot: surfplot(np.log(spatial_ps))

    return spatial_ps


def radial_profile(data, centre):

    # Get the coordinates of each tile
    y,x = np.indices((data.shape))

    # Find distance from centre for each tile
    r = np.sqrt((x - centre[0])**2 + (y - centre[1])**2)
    r = r.astype(np.int)

    # Count the value of each sample in each bin and the number of samples per bin
    tbin = np.bincount(r.ravel(), data.ravel())
    nr = np.bincount(r.ravel())

    # Normalise by number of occurences per bin
    radialprofile = tbin/nr

    return radialprofile

