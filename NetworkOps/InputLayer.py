import numpy as np
import scipy.sparse as sps


from NetworkOps.utilities.Euc_distance_calculator import distance_calc


def random_input_layer(n_units, n_inputs, connectivity):

    from NetworkOps.utilities.RandomBag import RandomBag

    # Initialise the network
    input_layer_connections = np.zeros( [n_inputs, n_units] )

    # Iterate over all neuron positions, and connect each of them to a random input neuron.
    random_input_neuron_bag = RandomBag(bagsize=1000, refill_method=lambda x: np.random.randint(0, n_inputs, x))
    random_in_0_1_bag = RandomBag(bagsize=1000, refill_method=lambda x: np.random.random(x))
    for i in range(n_units):
        if connectivity > random_in_0_1_bag.draw(1):
            input_layer_connections[random_input_neuron_bag.draw(1), i] = 1

    return sps.coo_matrix(input_layer_connections)


def RGF_input_layer(x, y, n_inputs, connectivity):
    from NetworkOps.utilities.RandomFields import gaussian_random_field
    from NetworkOps.utilities.RandomBag import RandomBag

    # Create a random Gaussian field
    RGF_RESOLUTION = 1000
    RGF = gaussian_random_field(alpha=3, size=RGF_RESOLUTION, flag_normalize=True)
    RGF += np.min(RGF)
    RGF /= np.mean(RGF)  # Now, the mean is 1
    RGF *= n_inputs*connectivity  # normalise by the number of neurons

    # Initialise the container
    input_layer_connections = np.zeros( [n_inputs, len(x)] )

    # Map the 1x1 positions over a RGF_RESOLUTIONxRGF_RESOLUTION discrete space
    x_int, y_int = (x*RGF_RESOLUTION).astype(int), (y*RGF_RESOLUTION).astype(int)

    # Iterate over all recurrent neuron positions, and connect each of them to a random input neuron.
    random_input_neuron_bag = RandomBag(bagsize=1000, refill_method=lambda x: np.random.randint(0, n_inputs, x))
    random_in_0_1_bag = RandomBag(bagsize=1000, refill_method=lambda x: np.random.random(x))
    for i, (xi, yi) in enumerate(zip(x_int, y_int)):
        if RGF[xi, yi] > random_in_0_1_bag.draw(1):
            input_layer_connections[random_input_neuron_bag.draw(1), i] = 1

    return input_layer_connections


def spatially_correlated_input_layer(x, y, periodic_boundaries,
                                     n_inputs, lam):
    """
    Creates a spatially correlated input layer (see Maas etc)
    Pick n_inputs points on a 1x1 square (uniformly at random). Then, connect each point at x,y with a probability
    based on the distance.
    The probability decays exponentially with distance - with a rate lambda.
    :param x:
    :param y:
    :param n_inputs:
    :param lam:
    :param periodic_boundaries:
    :return:
    """

    Xinput, Yinput = np.random.uniform(0, 1, n_inputs), np.random.uniform(0, 1, n_inputs)

    input_layer_connections = np.zeros( [n_inputs, len(x)] )

    for i in range(n_inputs):

        dist_i = distance_calc(x=x, y=y,
                               x_origin=Xinput[i],
                               y_origin=Yinput[i],
                               periodic=periodic_boundaries)

        probs_i = np.exp( - (dist_i/lam)**2 )

        input_layer_connections[i, :] = probs_i > np.random.uniform(0, 1, len(x))

    return input_layer_connections


def asdasss(n_reccurent, n_inputs, lam, periodic):

    x_rec = np.random.uniform(0, 1, n_reccurent)
    y_rec = np.random.uniform(0, 1, n_reccurent)

    input_layer = spatially_correlated_input_layer(x=x_rec, y=y_rec,
                                                   periodic_boundaries=periodic,
                                                   n_inputs=n_inputs,
                                                   lam=lam)

    import matplotlib.pyplot as plt
    for il in input_layer:
        plt.scatter(x_rec[il.astype(bool)],
                    y_rec[il.astype(bool)],
                    s=3)
    plt.show()

#asdasss(100,5,0.1,True)