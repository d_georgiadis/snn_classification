import numpy as np
from scipy.sparse import lil_matrix

from NetworkOps.utilities.Euc_distance_calculator import distance_calc
from NetworkOps.utilities.LloydIterations import lloyds_sampling


def RGG_network(n, e, periodic, lloyd_iter=0, x=None, y=None):

    assert(isinstance(n, int))

    # Random placement of neurons
    if x is None and y is None:
        if lloyd_iter == 0:
            x, y = np.random.uniform(0, 1, n), np.random.uniform(0, 1, n)
        elif lloyd_iter > 0:
            [x, y] = lloyds_sampling(n, lloyd_iter)
    n = len(x)

    theta = np.sqrt(e/(np.pi*(n-1)))

    # Make adj matrix (symmetric)
    A = lil_matrix(np.zeros((n, n)))
    for i in range(n):

        # Lambda connectivity model
        dist_i = distance_calc(x[i:], y[i:], x[i], y[i], periodic)
        connected_i = dist_i < theta

        # Symmetric A
        A[i, i:] = connected_i
        A[i:, i] = connected_i[:, np.newaxis]

        # No self loops
        A[i, i] = 0

    return A, x, y


def _plot_net(x_coor, y_coor, nodes, A):
    from matplotlib import pyplot as plt

    colors = np.squeeze(np.array(np.mean(A, axis=0)))
    plt.scatter(x_coor, y_coor, c=colors)
    # spt.voronoi_plot_2d([(x_coor[i], y_coor[i]) for i in range(nodes)])
    plt.figure(figsize=(10, 10))
    for i in range(nodes):
        for j in range(i):
            if A[i, j] == 1:
                plt.plot([x_coor[i], x_coor[j]], [y_coor[i], y_coor[j]], 'black')

    plt.savefig('plot_RGG.png', dpi=200)


def tasdadsdng_tess():

    n = 1000
    lams = [10]
    superseed = 2
    periodic = True

    for deg in lams:
        np.random.seed(superseed)
        print('Target mean degree', deg)
        net = RGG_network(n, deg, periodic)
        print('Achived mean degree ', np.sum(net[0]) / n)
        _plot_net(net[1], net[2], n, net[0])



