import numpy as np


class RandomBag:

    def __init__(self, bagsize,
                 refill_method=lambda n: np.random.uniform(0, 1, n) ):

        self._bagsize = int(bagsize)
        self._used_so_far = bagsize+1
        self._refill_method = refill_method
        self._random_stuff = np.zeros( int(bagsize) )

    def draw(self, howmany):

        remaining = self._bagsize - self._used_so_far
        if howmany > remaining:
            self._random_stuff = self._refill_method(self._bagsize)
            self._used_so_far = 0

        stuff = self._random_stuff[self._used_so_far:self._used_so_far+howmany]
        self._used_so_far += howmany

        return stuff


