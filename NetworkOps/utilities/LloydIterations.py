import matplotlib.pyplot as pl
import numpy as np
import scipy as sp
import scipy.spatial
import sys


def _in_box(n, bounds):
    return np.logical_and(np.logical_and(bounds[0] <= n[:, 0],
                                         n[:, 0] <= bounds[1]),
                          np.logical_and(bounds[2] <= n[:, 1],
                                         n[:, 1] <= bounds[3]))


def _voronoi(n, bounds):

    # Select towers inside the bounding box
    i = _in_box(n, bounds)
    padding = 0.5

    deltax = bounds[1] - bounds[0]
    deltay = bounds[3] - bounds[2]

    xpad = deltax * padding
    ypad = deltax * padding

    points_center = n[i, :]

    left_extrem = points_center[points_center[:, 0] < bounds[0] + xpad, :]
    right_extrem = points_center[points_center[:, 0] > bounds[1] - xpad, :]
    up_extrem = points_center[points_center[:, 1] > bounds[3] - ypad, :]
    down_extrem = points_center[points_center[:, 1] < bounds[2] + ypad, :]

    # Mirror points
    points_left = np.copy(right_extrem)
    points_left[:, 0] = points_left[:, 0] - deltax

    points_right = np.copy(left_extrem)
    points_right[:, 0] = points_right[:, 0] + deltax

    points_down = np.copy(up_extrem)
    points_down[:, 1] = points_down[:, 1] - deltay

    points_up = np.copy(down_extrem)
    points_up[:, 1] = points_up[:, 1] + deltay

    points = np.append(points_center,
                       np.append(np.append(points_left,
                                           points_right,
                                           axis=0),
                                 np.append(points_down,
                                           points_up,
                                           axis=0),
                                 axis=0),
                       axis=0)

    # Compute Voronoi
    vor = sp.spatial.Voronoi(points)

    # Filter regions
    regions = []
    for region in vor.regions:
        flag = True  # will we append the region or not? Is it an infinire region?
        for index in region:
            if index == -1:  # -1 index does not exist, signifies infinity.
                flag = False
                break
            else:
                x = vor.vertices[index, 0]
                y = vor.vertices[index, 1]

                eps = sys.float_info.epsilon

                # if not(bounds[0] - eps <= x and x <= bounds[1] + eps and bounds[2] - eps <= y and y <= bounds[3] + eps):
                #    flag = False
                #    break

        if region != [] and flag:
            regions.append(region)

    vor.filtered_points = points
    vor.filtered_regions = regions
    # print(len(regions), len(vor.regions))

    # pl.scatter(points[:, 0], points[:, 1], s=8)
    # pl.show()

    return vor


def _lloyd_iteration(points, bounds=np.array([0., 1., 0., 1.])):  # [x_min, x_max, y_min, y_max]

    def _centroid_region(vertices):
        # Polygon's signed area
        A = 0
        # Centroid's x
        C_x = 0
        # Centroid's y
        C_y = 0
        for i in range(0, len(vertices) - 1):
            s = (vertices[i, 0] * vertices[i + 1, 1] - vertices[i + 1, 0] * vertices[i, 1])
            A = A + s
            C_x = C_x + (vertices[i, 0] + vertices[i + 1, 0]) * s
            C_y = C_y + (vertices[i, 1] + vertices[i + 1, 1]) * s
        A = 0.5 * A
        C_x = (1.0 / (6.0 * A)) * C_x
        C_y = (1.0 / (6.0 * A)) * C_y
        return np.array([[C_x, C_y]])

    vor = _voronoi(points, bounds)

    # Compute and plot centroids
    centroids = np.zeros([len(vor.filtered_regions), 2])  # TODO use an array

    for i, region in enumerate(vor.filtered_regions):
        vertices = vor.vertices[region + [region[0]], :]
        centroid = _centroid_region(vertices)
        centroids[i, :] = centroid[0, :]

    return np.array(centroids)


def lloyds_sampling(n, iterations, bounds=np.array([0., 1., 0., 1.])):  # [x_min, x_max, y_min, y_max]

    points = np.random.rand(int(n), 2)

    for i in range(iterations):
        points = _lloyd_iteration(points, bounds=bounds)

    # only return points inside the box.
    x, y = points[:, 0], points[:, 1]

    in_the_box = np.logical_and(
        np.logical_and(x > 0, x < 1),
        np.logical_and(y > 0, y < 1)
    )

    return x[in_the_box], y[in_the_box]


def _testing():
    a, b = lloyds_sampling(1000, 20)

    fig, axarr = pl.subplots(2, 1, figsize=(14, 7))

    # Plot initial points
    # Points must be placed uniformly in space.
    axarr[0].hist(a, 50)
    axarr[1].hist(b, 50)

    pl.tight_layout()
    pl.show()

    return a, b


