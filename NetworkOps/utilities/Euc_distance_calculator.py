import numpy as np

def distance_calc(x, y, x_origin, y_origin, periodic):

    euc_len = np.sqrt((x_origin - x) ** 2 + (y_origin - y) ** 2)

    if periodic:

        threshold = 0.5

        # Borders
        if y_origin > 1 - threshold: euc_len_top = np.sqrt((x_origin - x) ** 2 + (y + 1 - y_origin) ** 2)
        else:                      euc_len_top = np.ones(len(x))  # Too far away

        if y_origin < threshold:     euc_len_bot = np.sqrt((x_origin - x) ** 2 + (y - 1 - y_origin) ** 2)
        else:                      euc_len_bot = np.ones(len(x))  # Too far away

        if x_origin > 1 - threshold: euc_len_rgt = np.sqrt((x + 1 - x_origin) ** 2 + (y_origin - y) ** 2)
        else:                      euc_len_rgt = np.ones(len(x))  # Too far away

        if x_origin < threshold:     euc_len_lft = np.sqrt((x - 1 - x_origin) ** 2 + (y_origin - y) ** 2)
        else:                      euc_len_lft = np.ones(len(x))  # Too far away

        # Corners
        if x_origin > 1 - threshold and y_origin < threshold: # bottom right corner
            euc_len_cor_rb = np.sqrt((x + 1 - x_origin) ** 2 + (y - 1 - y_origin) ** 2)
        else: euc_len_cor_rb = np.ones(len(x))  # Too far away

        if x_origin > 1 - threshold and y_origin > 1 - threshold: # top right corner
            euc_len_cor_rt = np.sqrt((x + 1 - x_origin) ** 2 + (y + 1 - y_origin) ** 2)
        else: euc_len_cor_rt = np.ones(len(x))  # Too far away

        if x_origin < threshold and y_origin > 1 - threshold: # top left corner
            euc_len_cor_lt = np.sqrt((x - 1 - x_origin) ** 2 + (y + 1 - y_origin) ** 2)
        else: euc_len_cor_lt = np.ones(len(x))  # Too far away

        if x_origin < threshold and y_origin < threshold: # bottom left corner
            euc_len_cor_lb = np.sqrt((x - 1 - x_origin) ** 2  + (y - 1 - y_origin) ** 2)
        else: euc_len_cor_lb = np.ones(len(x))  # Too far away

        # Take minima
        distance = np.min(np.vstack([euc_len, euc_len_top, euc_len_bot, euc_len_lft, euc_len_rgt,
                                     euc_len_cor_rb, euc_len_cor_rt, euc_len_cor_lb,euc_len_cor_lt]), axis=0)


        del euc_len_top, euc_len_bot, euc_len_lft, euc_len_rgt,\
            euc_len_cor_rb, euc_len_cor_rt, euc_len_cor_lb,euc_len_cor_lt  # to save ram

    else:

        distance = euc_len

    return distance