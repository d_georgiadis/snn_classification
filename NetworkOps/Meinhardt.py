import numpy as np
from scipy.sparse import lil_matrix, csr_matrix, dok_matrix

from NetworkOps.utilities.Euc_distance_calculator import distance_calc
from NetworkOps.utilities.LloydIterations import lloyds_sampling
from NetworkOps.utilities.RandomBag import RandomBag


def Meinhardt_network(n, inhibition,
                      synapse_sigma, synapse_strength,
                      l_e, density_e, l_i, density_i,
                      lloyd_iter, periodic, x=None, y=None):

    assert 0 <= density_e <= 1 and 0 <= density_i <= 1

    # Random placement of neurons
    if x is None and y is None:
        if lloyd_iter == 0:
            x, y = np.random.uniform(0, 1, n), np.random.uniform(0, 1, n)
        elif lloyd_iter > 0:
            [x, y] = lloyds_sampling(n, lloyd_iter)
            n = len(x)  # lloyds sampling may change the number of points (due to leakage)
    else:
        assert(n == len(x) == len(y))

    # Select who is inhibitory and who is excitatory
    n_inhibitory = int(inhibition*n)
    e_or_i = np.ones(n)
    e_or_i[:int(inhibition*n)] = -1

    # Assemble the graph
    A = lil_matrix((n, n), dtype=np.float32)

    # First the inhibitory connections
    for i in range(n_inhibitory):

        # Pick some points at random, since we have sparse connections
        random_filter = np.random.choice(a=[True, False], size=n, p=[density_i, 1-density_i])
        some_points_x, some_points_y = x[random_filter], y[random_filter]
        dist = distance_calc(some_points_x, some_points_y, x[i], y[i], periodic)

        # Connect, but no self loop
        A[i, random_filter] = -1. * (dist < l_i)
        A[i, i] = 0

    # Then the excitatory connections
    for i in range(n_inhibitory, n):

        # Pick some points at random, since we have sparse connections
        random_filter = np.random.choice(a=[True, False], size=n, p=[density_e, 1-density_e])
        some_points_x, some_points_y = x[random_filter], y[random_filter]
        dist = distance_calc(some_points_x, some_points_y, x[i], y[i], periodic)

        # Connect, but no self loop
        A[i, random_filter] = 1. * (dist < l_e)
        A[i, i] = 0

    # Randomise synaptic strength, if requested
    if synapse_sigma > 0:
        for i,_ in enumerate(A.data):
            bag_with_synapse_strengths = RandomBag(int(1e4),
                                                   refill_method=lambda x: np.random.normal(synapse_strength,
                                                                                            synapse_sigma, x))
            A.data[i] *= bag_with_synapse_strengths.draw(len(A.data[i]))
    else:
        A *= synapse_strength

    return A, x, y, e_or_i

