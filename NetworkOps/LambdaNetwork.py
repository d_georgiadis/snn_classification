import numpy as np
from scipy.sparse import lil_matrix

from NetworkOps.utilities.Euc_distance_calculator import distance_calc


def lam_network(n, lam, periodic):

    # Random placement of neurons
    x, y = np.random.uniform(0, 1, n), np.random.uniform(0, 1, n)

    # Make adj matrix (symmetric)
    A = lil_matrix(np.zeros((n, n)))
    for i in range(n):

        # Lambda connectivity model
        sigma_adj = lam / np.sqrt(n-1)
        dist_i = distance_calc(x[i:], y[i:], x[i], y[i], periodic)
        probs_i = np.exp( - (dist_i / sigma_adj) ** 2 )
        connected_i = np.random.uniform(0, 1, n-i) < probs_i

        # Symmetric A
        A[i, i:] = connected_i
        A[i:, i] = connected_i[:, np.newaxis]

        # No self loops
        A[i, i] = 0

    return A, x, y


def _plot_net(x_coor, y_coor, nodes, A):
    from matplotlib import pyplot as plt


    colors = np.squeeze(np.array(np.mean(A, axis=0)))
    plt.scatter(x_coor, y_coor, c=colors)
    # spt.voronoi_plot_2d([(x_coor[i], y_coor[i]) for i in range(nodes)])
    plt.figure(figsize=(10, 10))
    for i in range(nodes):
        for j in range(i):
            if A[i, j] == 1:
                plt.plot([x_coor[i], x_coor[j]], [y_coor[i], y_coor[j]], 'black')

    plt.savefig('plot_lambda.png', dpi=200)


def tasdadsdng_tess():

    n = 500
    lams = [1.3] # 1.3 results in about 5 connections per unit
    superseed = 1
    periodic = False#True

    for deg in lams:
        np.random.seed(superseed)
        print('Target mean degree', deg)
        net = lam_network(n, deg, periodic)
        print('Achived mean degree ', np.sum(net[0]) / n)
        _plot_net(net[1], net[2], n, net[0])



