import numpy as np
import h5py


def store(filepath, name, hyperdict):
    """
    Save a dictionary whose contents are only strings, np.float64, np.int64,
    np.ndarray, and other dictionaries following this structure
    to an HDF5 file.d
    """
    filename = filepath + '/' + name + '.hdf5'
    with h5py.File(filename, 'a') as h5file:
        recursively_save_dict_contents_to_group(h5file, '/', hyperdict)

    return filename


def recursively_save_dict_contents_to_group(h5file, path, dic):
    """
    Take an already open HDF5 file and insert the contents of a dictionary
    at the current path location. Can call itself recursively to fill
    out HDF5 files with the contents of a dictionary.
    """
    for key, item in dic.items():
        if isinstance(item, (np.ndarray, np.int32, np.int64, np.float64, np.bool_, list, str, bytes, int, float)) \
                or item is None:
            try:
                # here, this line would work - in most installations:
                # h5file.create_dataset(path + str(key), data=item, compression='gzip', compression_opts=3)
                # yet it does not always work because in some installation h5py has a check (see https://github.com/shoyer/h5netcdf/pull/13)
                # In short, h5py complains is you try to compress a scalar or an empty

                if type(item) is np.ndarray:
                    if len(item) > 1000:  # Otherwise compression is pointless
                        h5file.create_dataset(path + str(key), data=item,
                                              compression='gzip', compression_opts=5)
                    else:
                        h5file.create_dataset(path + str(key), data=item)
                else:
                    h5file.create_dataset(path + str(key), data=item)

            except:
                raise ValueError('Bad value for h5py storage OR file already exists.', type(item), item, np.shape(np.array(item)), key)

        elif isinstance(item, dict):
            recursively_save_dict_contents_to_group(h5file, path + key + '/', item)
        else:
            raise ValueError('Cannot save ' + str(type(item)) + ' type. Failed at key: ' + str(key))


def load(filename, special_orders=None):
    """
    Load a dictionary whose contents are only strings, floats, ints,
    numpy arrays, and other dictionaries following this structure
    from an HDF5 file.
    """
    with h5py.File(filename, 'r') as h5file:
        return recursively_load_dict_contents_from_group(h5file, '/', special_orders)


def recursively_load_dict_contents_from_group(h5file, path, special_orders):
    """
    Load contents of an HDF5 group. If further groups are encountered,
    treat them like dicts and continue to load them recursively.

    If special orders are given then they are applied. If special orders is None, then all is loaded normally.
    """
    ans = {}
    for key, item in h5file[path].items():
        if isinstance(item, h5py._hl.dataset.Dataset):
            if special_orders is None:
                ans[key] = item.value
            else:
                if key in special_orders.keys():
                    ans[key] = special_orders[key](item.value)
                else:
                    ans[key] = 'Not loaded.'
        elif isinstance(item, h5py._hl.group.Group):
            ans[key] = recursively_load_dict_contents_from_group(h5file, path + key + '/', special_orders)
    return ans

