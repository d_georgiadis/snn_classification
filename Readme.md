# Repository objective
The purpose of this repo is to implement and test a supervised Spiking Neural Net (SNN) algo, for:

 **the (multilabel) classification of high dimensional boolean timeseires** 

- This datatype (boolean high dimensional timeseries) will be referred to as a **'spiketrain'**.
- For a demo of the algorithm, **see Notebooks/1_PoissonClusters**
- For the class implementing the algorithm see **SNN.py**
- Besides the SNN layer, the algorithm is very cheap to run. The SNN layer can easily be implemented with dedicated hardware. This might be a good candidate for time critical applciations.

### Crucial details
- This is a side project in early development. I am still getting to a prototype model - the code is not production quality.
- I am the sole contributor of this repo.
- Most functions here are written from scratch: I am not using keras pytrorch or tensorflow. I do use sklearn to train perceptra though.
- The work presented here builds on two years worth of research on computational neuroscience. No hyperparameter value was picked arbitrarily - please contact me if you want details. 

### Dependancies
Conda virtual enviroment. Some functions also require the Brian2 package.

### How does the algo work?
The classification of timeseires requires that our ML method has some short of memory.
Desinging NNs with memory can be a daunting task: training becomes expensive and additional hyperparameters need to be picked. The idea behind this algorithm is to overcome these challenges by leveraging a dynamical system.

Consider the following game: 
The two of us are standing next to a lake, in which I am throwing stones. 
To win, you need to guess how many stones I threw in the water - but you are blindfolded.
On approach is to listen carefully and count.
The other approach is to open your eyes after I am done throwing stones and count the number of concetric circles over the lake's sruface. 
The later is the core principle of our approach.

In short: we first stimulate a dynamical system with the input timeseries, and then use the evetual state of the dynamical system as input to a perceptron classifier.

 

### What are the advantages of the algorithm?

A core idea here is to use a neural network is not trained only partially. Specifically, we will be using 3 layers - and only one of the will be trained. The other two layers are picked using a rule of thumb - which creates layers that do not need training.

The presented algorithm enjoys 4 prominent benefits:
- Low training costs: essentially each sample has to be processed just once, and then a perceptron has to be trained.
- Scalability: We can use the algorithm for multilabel classification. For each label added, we only need to train an additonal perceptron.
- Low memory complexity (compared to other timeserie calssifiers like LSTMs).
- Easily implemented on dedicated hardware.

Of course, the 'magic' here lies in the rule of thumb. We create a spiking neural network with specific topological properties. For details you can look up my publication "Pattern phase diagram of spiking neurons on spatial networks".


### How are we going to test the algo?
We will do 2 experiments to test the algo:
1. Toy data: Create random spiketrain data (coming from 2 classes) and cluster them (see Notebooks/1_PoissonClusters)
2. Spoken digit data: Create spectrograms from audio recordings of spoken digits. Use the spectrograms as inputs (this test is pending).

### What are the results so far?
- Experiment 1 is promising, we get good performance in and out of sample (>90%).
- The perceptron layers behaves peculiarly for certain inputs, I need to make sure that eveything works as it should.

### Next steps?
- Complete experiment 2 (make spectrograms).
- Investigate the perceptron
- Reduce technical debt (comment code, look for lurking performance bottlenecks).

### Project structure

- The implementation of the spiking neural network dynamics can be found in the SNN_setups subdir
- All network operations (for the creation of the spiking, input, and output layers) are found in NetworkOps
- All fucntion interfacing with the filesys (store/load, mkdir) functions are found in StorageOps
- Notebooks contains jupyter notebooks that investigate the algorithm's performance.
- DataOps has functions that allow for the creation of random spiketrains.

All other subdirs include fucntions used for the visualisation and analysis of the recurrent layer.
