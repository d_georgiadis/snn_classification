import brian2 as br
import numpy as np
from scipy import sparse as sps


def synchronous_simulation(sample,
                           capacity, ref_voltage, ref_tau, readout_tau,
                           drive_tau, ss_voltage, synapse_str,
                           v_store_for, sampling_rate, integration_timestep,
                           recurrent_layer, output_layer, input_layer,
                           ):
    """
    :param sample: (duration x n_inputs) np array.
    :param capacity:
    :param ref_voltage:
    :param ref_tau:
    :param readout_tau:
    :param drive_tau:
    :param ss_voltage:
    :param synapse_str:
    :param v_store_for:
    :param sampling_rate:
    :param integration_timestep:
    :param recurrent_layer: (N x N) sparse array, lil
    :param output_layer: (n_outputs x N) np dense array
    :param input_layer: (n_inputs x N) sparse coo matrix
    :return:
    """

    input_duration, n_inputs = sample.shape
    N = recurrent_layer.shape[0]
    n_out = output_layer.shape[0]

    ## THIS IS THE LIQUID
    # Destroy all previous brian2 objects  #####
    br.start_scope()

    # Configure neuron dynamics  #####
    # Make sure the units on the LHS and RHS of the eq are ok
    # The (unless refractory) statement forces voltage to (reset) when refractoring
    # The 1 after the : indicates that the LHS and RHS are dimensionless
    # All the parameters in the equation (I, tau) can be configured per neuron by G.I or G.tau.
    # dv/dt = (I-0.01*v)/tau : 1 (unless refractory)
    eqs = '''
       dv/dt = (vss-v )/(tau) : 1  (unless refractory)
       I: 1
       vss: 1
       tau: second
       x: 1
       y: 1
       '''
    # Configure input layer
    row, col = np.where(sample > 0)
    G_1 = br.SpikeGeneratorGroup(n_inputs,
                                 indices=col,
                                 times=row * sampling_rate * br.ms,
                                 dt=integration_timestep * br.ms)
    # Configure recurrent layer
    G_2 = br.NeuronGroup(N, eqs,
                         threshold='v>' + str(capacity),
                         reset='v = ' + str(ref_voltage),
                         refractory=ref_tau * br.ms,
                         method='exact',
                         dt=integration_timestep * br.ms)
    # Configure output layer
    G_3 = br.NeuronGroup(n_out, 'v:1', dt=integration_timestep * br.ms)

    # Configure per neuron parameters
    G_2.I = np.ones(N) * .1
    G_2.tau = np.ones(N) * drive_tau * br.ms
    # Random initial conditions to neuron voltage levels
    G_2.v = 0 * np.random.uniform(0, 1, N)  # np.zeros(N)  # 'rand()'
    G_2.vss = np.ones(N) * ss_voltage
    # Intiliase the readouts to 0
    G_3.v = np.zeros(n_out)

    # Create synapses  #####
    # Input synapses
    S_12 = br.Synapses(G_1, G_2, 'w:1',
                       on_pre='v_post += w',
                       dt=integration_timestep * br.ms)
    input_layer = input_layer.tocoo()
    S_12.connect(i=input_layer.row, j=input_layer.col)
    S_12.w = input_layer.data * synapse_str

    # Recurrent synapses
    S_22 = br.Synapses(G_2, G_2, 'w:1',
                       on_pre='v_post += w',
                       dt=integration_timestep * br.ms)
    recurrent_layer = recurrent_layer.tocoo()
    S_22.connect(i=recurrent_layer.col, j=recurrent_layer.row)
    S_22.w = recurrent_layer.data * synapse_str

    # Readout synapses
    S_23 = br.Synapses(G_2, G_3, 'w:1',
                       on_pre='v_post += w',
                       dt=integration_timestep * br.ms)
    output_layer = sps.coo_matrix(output_layer)
    S_23.connect(i=output_layer.col, j=output_layer.row)
    S_23.w = output_layer.data

    # Configure monitors  #####
    csts = br.SpikeMonitor(G_2, record=False)
    M_v_2 = br.StateMonitor(G_2, 'v', record=True, dt=v_store_for * br.ms)
    M_v_3 = br.StateMonitor(G_3, 'v', record=True)
    # M_s = br.SpikeMonitor(G_3)

    # Run simulation  #####
    br.run(input_duration * br.ms)

    # Condition output data
    output_spikes = np.zeros(M_v_3.v.shape)
    output_spikes[:, 1:] = np.diff(M_v_3.v)

    # TODO move the filtering to the repsective analysis  module. It should not be here. I should be storing pure spikes
    filtered_output = np.zeros(M_v_3.v.shape)
    adj_readout_tau = readout_tau / (output_spikes.shape[1] / input_duration)
    for i in range(1, output_spikes.shape[1]):
        filtered_output[:, i] = filtered_output[:, i - 1] * (1 - adj_readout_tau) + output_spikes[:, i]

    print(type(M_v_2.v))
    print(M_v_2.v.shape)

    return M_v_2, filtered_output, csts
