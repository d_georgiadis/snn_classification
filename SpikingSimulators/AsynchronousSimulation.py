import numpy as np
from scipy import sparse as sps

import matplotlib.pyplot as plt


def asynchronous_simulation(sample,
                            capacity, ref_voltage,
                            readout_tau,
                            use_phase,
                            drive_tau, ss_voltage,
                            v_store_for, sampling_rate,
                            recurrent_layer, output_layer, input_layer
                            ):

    ### Preprocessing
    input_duration, n_inputs = sample.shape
    N = recurrent_layer.shape[0]
    n_out = output_layer.shape[0]
    # Storage
    voltage_storage = np.zeros( (int(np.floor(input_duration/v_store_for)), N) )
    cascade_sizes = np.zeros(input_duration)
    output = np.zeros( (input_duration, n_out) )
    # States
    voltage = np.zeros(N, dtype=np.float16)
    refractory = np.zeros(N, dtype=bool)

    ### Apply input layer
    reccurent_layer_drive = sps.coo_matrix(sample).dot(input_layer).tocsr()

    ### Apply recurrent layer, and output layer
    storage_counter = 0
    # Iterate over time
    adj_leak = sampling_rate * drive_tau

    for t, drive_t in enumerate(reccurent_layer_drive):
        # Leaky dynamics
        voltage += (ss_voltage - voltage) * adj_leak
        # Drive from input layer
        voltage[drive_t.indices] += drive_t.data
        # Check for spiking
        any_violators = (voltage[drive_t.indices] > capacity).any()

        # Run cascading
        if any_violators:
            # Find violators
            violators_idx = np.where(voltage > capacity)[0]

            # Continue the cascade for as long as violators exist
            while len(violators_idx) > 0:
                # Find violators
                refractory[violators_idx] = 1
                # Get the change of voltages
                delta_volt = np.array(recurrent_layer[violators_idx].sum(axis=0))[0]
                delta_volt[refractory] = 0
                # Apply change of voltages
                voltage += delta_volt
                # Rest violators
                voltage[violators_idx] = ref_voltage
                # Find new violators
                violators_idx = np.where(voltage > capacity)[0]

            # Log cascade size
            cascade_sizes[t] = np.sum(refractory)

            # Update output layer
            #if use_phase:
            #    output[t, :] = output_layer.dot(voltage)
            #else:
            output[t, :] += output_layer.dot(refractory)

            # Reset refractoriness
            refractory *= False

        # Log voltages if needed
        if t % v_store_for == 0:
            voltage_storage[storage_counter, :] = voltage.copy()
            storage_counter += 1

    return voltage_storage, output, cascade_sizes
