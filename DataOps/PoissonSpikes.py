import numpy as np


def iid_spiketrain(sample_dim, duration, sample_density, sample_seed):

    n_input_spikes = int(sample_density * sample_dim * duration)

    sample = np.zeros((int(duration), int(sample_dim)), dtype=np.bool)

    np.random.seed(sample_seed)
    sample[np.random.randint(0, duration, n_input_spikes),
           np.random.choice(np.arange(sample_dim), n_input_spikes)] = 1

    return sample


def sine_modulated_spiketrain(sample_dim, n_waves, duration, sample_density):

    assert(sample_density < .5)

    single_sine = np.sin( np.linspace(1, n_waves * 2 * np.pi, duration) )

    # Manipulate the sine wave so that we get a fixed sample density
    single_sine = 1 + single_sine  # Now the min is 0, and the max 2

    # If the max is one we would get a density of 0.5
    single_sine *= sample_density   # Here, the maximum is set to 2*sample_den.

    sine = np.tile(single_sine, (sample_dim, 1)).transpose()

    sample = sine > np.random.uniform(0, 1, sine.shape)

    return sample


def temporally_distort_sample(sample, D):
    sample = sample.copy()

    n_spikes = np.sum(sample)
    duration, n_inputs = sample.shape

    random_temporal_movements = np.random.poisson(D, n_spikes) * np.random.choice([-1, 1], n_spikes)

    when, where = sample.nonzero()

    new_when, new_where = when.copy()+random_temporal_movements, where

    filter = (new_when > 0) & (new_when < duration-1)
    new_where = new_where[filter]
    new_when = new_when[filter]

    noisy_sample = np.zeros((duration, n_inputs))

    noisy_sample[new_when, new_where] = 1

    return noisy_sample


def asdasda():
    import matplotlib.pyplot as plt
    a = iid_spiketrain(sample_dim=20, duration=100, sample_density=0.01)

    b = temporally_distort_sample(a, 1)
    c = temporally_distort_sample(a, 1)
    d = temporally_distort_sample(a, 1)
    e = temporally_distort_sample(a, 1)

    fig,axarr = plt.subplots(1, 5, figsize=(15, 5))
    axarr[0].imshow(a)
    axarr[1].imshow(b)
    axarr[2].imshow(c)
    axarr[3].imshow(d)
    axarr[4].imshow(e)
    plt.show()