import numpy as np
from scipy import sparse as sps


def temporally_distort_sample(sample, D):

    n_spikes = len(sample.nonzero()[0])
    duration, sys_size = sample.shape

    random_temporal_movements = np.random.poisson(D, n_spikes) * np.random.choice([-1, 1], n_spikes)

    when, where = sample.nonzero()

    new_when = np.clip(random_temporal_movements + when, 0, duration-1)

    noisy_sample = sps.lil_matrix((duration, sys_size), dtype=np.bool)

    noisy_sample[new_when, where] = 1

    return noisy_sample